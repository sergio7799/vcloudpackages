#!/bin/bash

npm config set strict-ssl false

# declare -A CRISTIAN

LOCAL_VERSION=$(node -p -e "require('./package.json').version"); # Version in package.json
REMOTE_VERSION=$(npm view . version); # Version was published
REMOTE_POSTFIX=$(npm view .@${LOCAL_VERSION} version)

YELLOW='\033[1;33m'
NC='\033[0m' # No Color

echo $LOCAL_VERSION
echo $REMOTE_VERSION

if [ "${REMOTE_POSTFIX}" ] || [ "${LOCAL_VERSION}" \< "${REMOTE_POSTFIX}" ]
then # if/then branch
  	npm install
    echo \#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#
    echo "WARNING: The version was not changed, this will not be published."
    echo \#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#
    printf "I ${YELLOW}love${NC} Stack Overflow\n"
else # else branch
	npm install
    npm publish
    echo "The new version was published successfully."
fi

